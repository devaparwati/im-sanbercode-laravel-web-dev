<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    @extends('layouts.table')

    @section('title')
        Halaman Pendaftaran
    @endsection
    

    @section('sub-title')
        Halaman Pendaftaran
    @endsection

    @section('content')
        
    <h1>Buat Acount Baru</h1>
    <form action="/welcome" method="post" >
        @csrf
        <label>Fist Name:</label><br>
        <input type="text" name="fname"><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="lname"><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="Gender" id="" value="1">Male<br>
        <input type="radio" name="Gender" id="" value="2">Female<br>
        <input type="radio" name="Gender" id="" value="3">Other<br>

        <label>Nationality:</label><br>
        <select name="Nationality:" id="">
        <option value="1">Indonesian</option>
        <option value="2">Malaysian</option>
        <option value="3">Singaporean</option>
        <option value="4">Japanese</option>
        </select><br>

        <label>Language Spoken</label><br>
        <input type="checkbox" name="Language Spoken" value="1">Bahasa Indonesia<br>
        <input type="checkbox" name="Language Spoken" value="2">English<br>
        <input type="checkbox" name="Language Spoken" value="3">Other<br>

        <label>Bio:</label><br>
        <textarea nama="Bio:" id="" close="30" rows="10"></textarea><br><br>



        <input type="submit" value="kirim">
    </form>
    @endsection

</body>
</html>