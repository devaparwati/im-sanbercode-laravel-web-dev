<?php
 function ubah_huruf($string){
   //kode di sini 
   $result = "";
    for ($i = 0; $i < strlen($string); $i++) {
        $char = $string[$i];
        if ($char >= 'a' && $char < 'z') {
            $char = chr(ord($char) + 1);
        } else if ($char == 'z') {
            $char = 'a';
        }
        $result .= $char;
    }
    return $result;
}


// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>