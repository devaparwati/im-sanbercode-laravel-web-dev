<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AuthController extends Controller
{
    public function register()
    {
        return view ('page.register');
    }
    public function kirim(Request $request)
    {
        //dd($request->all());
         $namaDepan = $request['fname'];
         $namaBelakang = $request['lname'];
     
         return view('page.welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
        
   
    }
}
